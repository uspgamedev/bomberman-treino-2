extends Node2D

export (PackedScene) var Player1
export (PackedScene) var Player2
export (PackedScene) var Player3
export (PackedScene) var Player4


# Called when the node enters the scene tree for the first time.
func _ready():
	if NumberPlayers.NumberOfPlayers == 2:
		var player1 = Player1.instance()
		var player2 = Player2.instance()
		
		player1.position = Vector2(128,128)
		player2.position = Vector2(1154,771)
		
		add_child(player1)
		add_child(player2)
		
		
		
	if NumberPlayers.NumberOfPlayers == 3:
		var player1 = Player1.instance()
		var player2 = Player2.instance()
		var player3 = Player3.instance()
		
		player1.position = Vector2(128,128)
		player2.position = Vector2(1154,771)
		player3.position = Vector2(130,768)
		
		add_child(player1)
		add_child(player2)
		add_child(player3)
		
		
	if NumberPlayers.NumberOfPlayers == 4:
		var player1 = Player1.instance()
		var player2 = Player2.instance()
		var player3 = Player3.instance()
		var player4 = Player4.instance()
		
		player1.position = Vector2(128,128)
		player2.position = Vector2(1154,771)
		player3.position = Vector2(130,768)
		player4.position = Vector2(1152,128)
		
		add_child(player1)
		add_child(player2)
		add_child(player3)
		add_child(player4)
	
	
	
	
	
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
