extends Node2D

export (int) var QualSprite 

var aparecer = true
var passou = false


func Comparar_lista_grupo(lista , grupo):
	var comparacao = false
	var i = 0
	if lista.size() > 0:
		while i <= lista.size() - 1:
			if lista[i].is_in_group(grupo):
				comparacao = true
			i += 1
	return comparacao


func _on_AparecerExplosao_timeout():
	if not passou:
		get_node("Explosao/Sprite" + str(QualSprite) + ":CanvasItem").visible = aparecer
		if aparecer:
			get_node("Explosao/Sprite" + str(QualSprite) + "/AnimationPlayer" + str(QualSprite)).play("Sprite" + str(QualSprite))
		passou = true
	
	pass # Replace with function body.


func _on_LigarDeteco_timeout():
	$Explosao/CollisionShape2D.disabled = false



func _on_Explosao_area_entered(area):
	var passou = false
	if not passou:
		if Comparar_lista_grupo($Explosao.get_overlapping_areas(), "Fixa"):
			aparecer = false
			passou = true
		else:
			aparecer = true
			passou = true
	pass # Replace with function body.


func _on_DesligarDeteccao_timeout():
	$Explosao/CollisionShape2D.disabled = true
	pass # Replace with function body.
