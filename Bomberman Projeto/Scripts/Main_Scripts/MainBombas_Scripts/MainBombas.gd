extends Node2D
export (String) var PlayerNumber
export (PackedScene) var explosao
var raio = 0
var k = 64
var Raio_direita = 0
var Raio_esquerda = 0
var Raio_cima = 0
var Raio_baixo = 0
func _ready():
	pass
	
	
	

func _on_Timer_timeout():
	queue_free()



func _on_DelayPraVasculhar_timeout():
	Raio_direita = get_node("./StaticBody2D/TesteDireita").raio_direita
	Raio_esquerda = get_node("./StaticBody2D/TesteEsquerda").raio_esquerda
	Raio_baixo = get_node("./StaticBody2D/TesteBaixo").raio_baixo
	Raio_cima = get_node("./StaticBody2D/TesteCima").raio_cima
	var explosaoCentral = explosao.instance()
	add_child(explosaoCentral)
	explosaoCentral.QualSprite = 1
	explosaoCentral.position = Vector2(-1,0)
	
	print ("Raio_direita = " , Raio_direita)
	while Raio_direita > 0:
		var explosaoNode = explosao.instance()
		add_child(explosaoNode)
		explosaoNode.position = Vector2(k,0)
		if Raio_direita == 1:
			explosaoNode.QualSprite = 3
		else:
			explosaoNode.QualSprite = 2
		Raio_direita -= 1
		k += 64
	
	k = 64
	
	while Raio_esquerda > 0:
		var explosaoNode = explosao.instance()
		add_child(explosaoNode)
		explosaoNode.position = Vector2(-k,0)
		if Raio_esquerda == 1:
			explosaoNode.QualSprite = 7
		else:
			explosaoNode.QualSprite = 6
		Raio_esquerda -= 1
		k += 64
	
	k = 64
	
	while Raio_baixo > 0:
		var explosaoNode = explosao.instance()
		add_child(explosaoNode)
		explosaoNode.position = Vector2(0,k)
		if Raio_baixo == 1:
			explosaoNode.QualSprite = 9
		else:
			explosaoNode.QualSprite = 8
		Raio_baixo -= 1
		k += 64
	
	k = 64
	
	while Raio_cima > 0:
		var explosaoNode = explosao.instance()
		add_child(explosaoNode)
		explosaoNode.position = Vector2(0,-k)
		if Raio_cima == 1:
			explosaoNode.QualSprite = 5
		else:
			explosaoNode.QualSprite = 4
		Raio_cima -= 1
		k += 64


func Comparar_lista_grupo(lista , grupo):
	var comparacao = false
	var i = 0
	if lista.size() > 0:
		while i <= lista.size() - 1:
			if lista[i].is_in_group(grupo):
				comparacao = true
			i += 1
	return comparacao



## O node que eu quero esticar e um Vector2 com a direção ex: Mover_Esticar(get_node(".."),Vector2(1,0))
func Mover_Esticar(node, direcao): 
	node.position += direcao*32
	
	if direcao.x != 0:
		node.scale.x += 1
		
	if direcao.y != 0:
		node.scale.y += 1
