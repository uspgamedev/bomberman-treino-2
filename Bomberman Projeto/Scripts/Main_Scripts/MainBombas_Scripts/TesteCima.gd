extends Area2D

export (int) var raio_cima
var raio = 0
var parar = false
onready var playerNumber = get_node("../..").PlayerNumber

func _ready():
	pass
	

	
	
func _physics_process(delta):
	var raio_max = get_node("/root/Node2D/Player " + playerNumber).Raio_max
	if Comparar_lista_grupo(get_overlapping_areas() , "ParaExplosao") and not parar and raio < raio_max:
			parar = true
			raio += 1
	if raio < raio_max and not parar:
		Mover_Esticar(self, Vector2(0,-1))
		raio += 1
	raio_cima = raio


func Mover_Esticar(node, direcao): 
	node.position += direcao*32
	
	if direcao.x != 0:
		node.scale.x += 1
		
	if direcao.y != 0:
		node.scale.y += 1


func Comparar_lista_grupo(lista , grupo):
	var comparacao = false
	var i = 0
	if lista.size() > 0:
		while i <= lista.size() - 1:
			if lista[i].is_in_group(grupo):
				comparacao = true
			i += 1
	return comparacao
