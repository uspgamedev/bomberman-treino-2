extends TileMap
export (PackedScene) var ParedeFixaArea

const AREA_TILE = 0 # your id might be different

func _ready():
	for cell in get_used_cells_by_id(AREA_TILE):
		var new_area = ParedeFixaArea.instance()
		new_area.global_position = map_to_world(cell) + Vector2(32,32)
	
		add_child(new_area)

