extends Control




func _on_Play_pressed():
	get_tree().change_scene("res://Scenes/Main.tscn")


func _on_2P_pressed():
	NumberPlayers.NumberOfPlayers = 2
	if get_node("3P").pressed or get_node("4P").pressed:
		get_node("3P").pressed = false
		get_node("4P").pressed = false
	else:
		get_node("2P").pressed = true



func _on_3P_pressed():
	NumberPlayers.NumberOfPlayers = 3
	if get_node("2P").pressed or get_node("4P").pressed:
		get_node("2P").pressed = false
		get_node("4P").pressed = false
	else:
		get_node("3P").pressed = true



func _on_4P_pressed():
	NumberPlayers.NumberOfPlayers  = 4
	if get_node("2P").pressed or get_node("3P").pressed:
		get_node("2P").pressed = false
		get_node("3P").pressed = false
	else:
		get_node("4P").pressed = true


func _on_HowToPlay_pressed():
	pass # Replace with function body.


func _on_Exit_pressed():
	get_tree().quit()
	pass # Replace with function body.
