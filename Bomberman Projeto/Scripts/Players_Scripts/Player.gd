extends KinematicBody2D

onready var animation_tree = get_node("AnimationTree")
onready var animation_mode = animation_tree.get("parameters/playback")
export (String) var MyNumber

const errosAceitos = [0,1,2,3,4,5,6,7,8,9,10,11,12,13, 14, 15, 16,128,127,126,125,124,123,122,121,120,119,118,117,116,115,114,113,112]
const errosBaixo = [128,127,126,125,124,123,122,121,120,119,118,117,116,115,114,113,112]
var direcao = Vector2(0,0)
export (int) var velocidade 
export (PackedScene) var Bomba
export (int) var NumeroBombas
export (int) var Raio_max = 1
var Morto = false


func _ready():
	animation_mode.start("Idle")
	pass
	


func _physics_process(delta):
	if not Morto:
		self.position = Vector2( round(self.position.x), round(self.position.y) )
		direcao = Vector2(0,0)
	
		if Input.is_action_pressed("Cima " + MyNumber) and Comparar_ou(int(self.position.x) % 128, errosAceitos):
			direcao = Vector2(0,-1)
			
			if Comparar_ou(int(self.position.x) % 128, errosBaixo):
				self.position.x += 128 - int(self.position.x) % 128
				
			self.position.x -= int(self.position.x) % 128
			
			
			
		if Input.is_action_pressed("Baixo " + MyNumber) and Comparar_ou(int(self.position.x) % 128, errosAceitos):
			direcao = Vector2(0,1)
			
			if Comparar_ou(int(self.position.x) % 128, errosBaixo):
				self.position.x += 128 - int(self.position.x) % 128
				
			self.position.x -= int(self.position.x) % 128
			
		if Input.is_action_pressed("Direita " + MyNumber) and Comparar_ou(int(self.position.y) % 128, errosAceitos):
			direcao = Vector2(1,0)
			
			if Comparar_ou(int(self.position.y) % 128, errosBaixo):
				self.position.y += 128 - int(self.position.y) % 128
				
			self.position.y -= int(self.position.y) % 128
		
		if Input.is_action_pressed("Esquerda " + MyNumber) and Comparar_ou(int(self.position.y) % 128, errosAceitos):
			direcao = Vector2(-1,0)
			
			if Comparar_ou(int(self.position.y) % 128, errosBaixo):
				self.position.y += 128 - int(self.position.y) % 128
				
			self.position.y -= int(self.position.y) % 128
		
		
		
		if Input.is_action_just_pressed("Espaço " + MyNumber) and get_tree().get_nodes_in_group("Bomba" + MyNumber).size() < NumeroBombas :
			var BombaNode = Bomba.instance()
			BombaNode.position = Vector2(round(self.position.x/64)*64 , round(self.position.y/64)*64)
			BombaNode.add_to_group("Bomba" + MyNumber)
			BombaNode.PlayerNumber = MyNumber
			get_parent().add_child(BombaNode)
			
	
	
		animation_tree.set("parameters/Walk/blend_position", direcao)
	
		if direcao != Vector2(0,0) and animation_mode.get_current_node() == "Idle":
			animation_mode.travel("Walk")
			animation_tree.set("parameters/Idle/blend_position", direcao)

		if direcao == Vector2(0,0) and animation_mode.get_current_node() == "Walk":
			animation_mode.travel("Idle")
		
		
		move_and_slide(direcao*velocidade)
	

func Comparar_ou(inteiro ,lista):
	var comparacao = false
	var i = 0
	while i <= lista.size() - 1:
		if inteiro == lista[i] or inteiro == -lista[i]:
			comparacao = true
		i += 1
	return comparacao


func _on_Area2D_area_entered(area):
	if Comparar_lista_grupo($Area2D.get_overlapping_areas(), "Explosao") and not Morto:
		Morto = true
		$AnimationPlayer.play("Die", -1, 0.5)
		$AnimationTree.active = false
		yield(get_tree().create_timer(5.0), "timeout")
		queue_free()
	if Comparar_lista_grupo($Area2D.get_overlapping_areas(), "Upgrade"):
		var qualupgrade = Procurar_grupo($Area2D.get_overlapping_areas(), "Upgrade").get_parent().QualUpgrade
		if qualupgrade == 1:
			velocidade += 50
			
		if qualupgrade == 2:
			Raio_max += 1
			
		if qualupgrade == 3:
			NumeroBombas += 200
		
		Procurar_grupo($Area2D.get_overlapping_areas(), "Upgrade").get_parent().queue_free()
		
		
		print(Procurar_grupo($Area2D.get_overlapping_areas(), "Upgrade").get_parent().QualUpgrade)
		
	



func Comparar_lista_grupo(lista , grupo):
	var comparacao = false
	var i = 0
	while i <= lista.size() - 1:
		if lista[i].is_in_group(grupo):
			comparacao = true
		i += 1
	return comparacao


func Procurar_grupo(lista , grupo):
	var achei
	var comparacao = false
	var i = 0
	while i <= lista.size() - 1:
		if lista[i].is_in_group(grupo):
			achei = true
			return lista [i]
		i += 1
	if not achei:
		return 0
